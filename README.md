# Ansible Ubuntu #

A collection of ansible playbooks for Ubuntu Linux distributions

> The ansible playbooks in this repository has been tested with ansible 2.9.6

## Ubuntu Version Compatibility

| Ubuntu Distribution | Branch Name |
| ------------------- | ----------- |
| Focal Fossa 20.04   | master      |
| Bionic Beaver 18.04 | 18.04       |
| Xenial Xerus 16.04  | 16.04       |

## Preparation ##

* Install ansible version `2.9.6` or above.
* Please update the ansible inventory file : ``/hosts``
* Please update the variables in ``vars/resource_allocation.yml``
* If it's necessary, you can also update the variables in ``vars/global.yml``
* Add your local's public key ``~/.ssh/id_rsa.pub`` into the remote server's authorized keys : ``~/.ssh/authorized_keys``
* Run the following command in the remote server : ``sudo apt-get install python3-minimal aptitude python-apt``

## Basic usage ##

``ansible-playbook -i hosts <role>.yml``

This is the list of roles available in current package :
* common
* nginx
* php
* mysql
* memcached
* varnish
* _~~redis~~_

## Installed softwares

| Software   | Version |
| ---------- | ------- |
| nginx      | 1.17.10 |
| MySQL      | 8.0     |
| PHP        | 7.4     |
| memcached  | 1.5.22  |
| varnish    | 6.2.1   |
| phpmyadmin | 5.0.2   |
