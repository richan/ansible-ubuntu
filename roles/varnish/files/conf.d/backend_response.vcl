vcl 4.0;

sub vcl_backend_response {
    call handle_backend_response;

    if (beresp.http.X-Varnish-Uncacheable ~ "1") {
        set beresp.http.Cache-Control = "no-cache";
        set beresp.uncacheable = true;
        return (deliver);
    }

    # Force to disable cache on specific routes
    if (bereq.url ~ "^\/(backend|secret\/login|captcha|elfinder)") {
        set beresp.uncacheable = true;
        return (deliver);
    }

    if (beresp.http.X-Varnish-Cacheable ~ "1") {
        unset beresp.http.set-cookie;
        unset beresp.http.pragma;
    }
}
