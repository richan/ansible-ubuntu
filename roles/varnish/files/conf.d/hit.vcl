vcl 4.0;

sub vcl_hit {
    # Custom Headers for debug purposes
    set req.http.X-Cache-TTL-Remaining = obj.ttl;
    set req.http.X-Cache-Age = obj.keep - obj.ttl;
    set req.http.X-Cache-Grace = obj.grace;
}
