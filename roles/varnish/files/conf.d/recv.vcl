vcl 4.0;

sub vcl_recv {
    set req.backend_hint = director.backend();

    call normalize_request;

    # You can decide to remove cookies
    # when a user is accessing any specific routes
    #if (req.url ~ "^\/(home|about)") {
    #    unset req.http.Cookie;
    #}

    if (req.url ~ "^\/(backend|secret\/login|captcha|elfinder)") {
        return (pass);
    }

    if (req.http.Cookie) {
        set req.http.X-Cookie = req.http.Cookie;
        unset req.http.Cookie;
    }
}
